#pragma once
#include "TaskManager.h"
class MyFileReader
{
public:
	std::vector<Task> LoadDataToClass(std::string);
	void WriteDataToFile(std::string, Task *, int, int, int);
	MyFileReader();
	~MyFileReader();
};

 