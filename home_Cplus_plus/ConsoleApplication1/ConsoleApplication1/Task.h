#pragma once
#include<iostream>

class Task
{
private:
	int Id;
	std::string Name;
	std::string Description;
	time_t Data_time;
public:
	Task();
	~Task();
	std::string GetName();
	std::string GetDescription();
	time_t GetTime();
	int GetId();
	void SetId(int);
	void SetName(std::string);
	void SetDescription(std::string);
	void SetTime(time_t);
};

