#pragma once
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include "MyFileReader.h"
#include "TaskManager.h"

std::vector<Task> MyFileReader::LoadDataToClass(std::string fileName)
{
	std::ifstream infile;
	infile.open(fileName);
	int size=0;
	if (infile) {
		// get length of file:
		infile.seekg(0, infile.end);
		size = infile.tellg();
		infile.seekg(0, infile.beg);
	}	
	char *buff = new char[size];
	if (!infile)
	{
		std::cout << "Unable to open input file. " << std::endl;
	}
	infile.read(buff, size);
	char *line_lecsem, *last_lecsem, *next_lecsem;
	std::vector<char*> temp_line_array;
	std::vector<char*> temp_line_value;
  /* ����������� ����� ������� */
	line_lecsem=strtok_s(buff, "\n", &next_lecsem);
	while (line_lecsem != NULL)
	{
		temp_line_array.push_back(line_lecsem);
	    /* ����� �������� ������� */
		line_lecsem=strtok_s(NULL, "\n", &next_lecsem);
	}
	std::vector<Task> ListTask;
	Task temp;
	for (int i = 0; i < temp_line_array.size()-1; i++)
	{		
		last_lecsem=strtok_s(temp_line_array[i], "|", &next_lecsem);
		while (last_lecsem != NULL)
		{
			temp_line_value.push_back(last_lecsem);
			 /* ����� �������� ������� */
			last_lecsem=strtok_s(NULL, "|", &next_lecsem);
		}
		temp.SetId(atoi(temp_line_value[0]));
		temp.SetName(temp_line_value[1]);
		std::string Str = std::string(temp_line_value[2]);
		temp.SetDescription(Str);
		temp.SetTime(atol(temp_line_value[3]));		
		temp_line_value.pop_back();
		temp_line_value.pop_back();
		temp_line_value.pop_back();
		temp_line_value.pop_back();
		ListTask.push_back(temp);
	}
	infile.close();
	return ListTask;	
}

void MyFileReader::WriteDataToFile(std::string fileName,Task *task, int year,int month,int day)
{	
	struct tm timeinfo;
	time_t rawtime;
	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);
	timeinfo.tm_year = year - 1900;
	timeinfo.tm_mon = month - 1;
	timeinfo.tm_mday = day;
	task->SetTime(mktime(&timeinfo));
	std::ofstream fout(fileName, std::ios::app); 
		if (!fout.is_open())
		{
			std::cout << "ERROR\n";
		}
		else {			
			fout << task->GetId()<<"|"<<task->GetName()<<"|"<<task->GetDescription()<<"|"<<task->GetTime()<<std::endl; 
			fout.close(); 		
		}
		fout.close();
}

MyFileReader::MyFileReader()
{
}


MyFileReader::~MyFileReader()
{
}
