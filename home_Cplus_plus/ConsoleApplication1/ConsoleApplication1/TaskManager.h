#pragma once
#include<iostream>
#include<time.h>
#include<list>
#include<vector>
#include<istream>
#include <string>
#include <sstream>
#include "Task.h"
#define _CRT_SECURE_NO_WARNINGS

class TaskManager
{
	
public:  
	TaskManager();
	~TaskManager();	
	void printAll(const std::vector<Task> &);
	void printTaskToday(const std::vector<Task> &);
	void printTaskTomorow(const std::vector<Task> &);
	void printTaskForDate(const std::vector<Task> &, int,int,int);
};

