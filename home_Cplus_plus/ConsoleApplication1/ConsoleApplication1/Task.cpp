#include "stdafx.h"
#include "Task.h"


Task::Task()
{
}


Task::~Task()
{
}
std::string Task::GetName()
{
	return Name;
}

std::string Task::GetDescription()
{
	return Description;
}

time_t Task::GetTime()
{
	return Data_time;
}

int Task::GetId()
{
	return Id;
}

void Task::SetId(int id)
{
	Id = id;
}

void Task::SetName(std::string name)
{
	Name = name;
}

void Task::SetDescription(std::string desc)
{
	Description = desc;
}

void Task::SetTime(time_t dt)
{
	Data_time = dt;
}

