#pragma once
#include "stdafx.h"
#include <string>
#include "TaskManager.h"

TaskManager::TaskManager()
{
}


TaskManager::~TaskManager()
{
	
}


void TaskManager::printAll(const  std::vector<Task> &tasks)
{
	std::cout << "------------------All Task-----------------"<< std::endl;
	for each (Task task in tasks)
	{		
		time_t taskdata = task.GetTime();
		tm ltm_task;
		localtime_s(&ltm_task, &taskdata);
		std::cout <<"Name:\t"+task.GetName() << std::endl;	
		std::cout << "Description:\t" + task.GetDescription() << std::endl;
		std::cout << "Date:\t"<< ltm_task.tm_year+1900 <<"."<<ltm_task.tm_mon+1<<"."<<ltm_task.tm_mday<< std::endl;
		std::cout << std::endl;
	}
}

 void TaskManager::printTaskToday(const std::vector<Task> &tasks)
{
	std::cout << "--------------------Task today:------------------" << std::endl;
	int counter = 0;
	for each (Task task in tasks)
	{
		time_t taskdata = task.GetTime();
		time_t now = time(0);		
		tm ltm_task;
		localtime_s(&ltm_task, &taskdata);
		tm ltm_today; 
		localtime_s(&ltm_today, &now);
		if (ltm_task.tm_year == ltm_today.tm_year && ltm_task.tm_mon == ltm_today.tm_mon && ltm_task.tm_mday == ltm_today.tm_mday)
		{
			std::cout << "Name:\t" + task.GetName() << std::endl;
			std::cout << "Description:\t" + task.GetDescription() << std::endl;
			std::cout << std::endl;
			counter++;
		}		
	}
	if (counter == 0)
	{
		std::cout << "No task today" << std::endl;
	}
}

void TaskManager::printTaskTomorow(const std::vector<Task> &tasks)
{
	int counter = 0;
	std::cout << "----------------Task tomorrow:------------------" << std::endl;
	for each (Task task in tasks)
	{
		time_t taskdata = task.GetTime();
		time_t now = time(0);
		tm ltm_task;
		localtime_s(&ltm_task, &taskdata);
		tm ltm_today;
		localtime_s(&ltm_today, &now);
		if (ltm_task.tm_year == ltm_today.tm_year && ltm_task.tm_mon == ltm_today.tm_mon && ltm_task.tm_mday == ++ltm_today.tm_mday)
		{
			std::cout << "Name:\t" + task.GetName() << std::endl;
			std::cout << "Description:\t" + task.GetDescription() << std::endl;
			std::cout << std::endl;
			counter++;
		}		
	}	
	if (counter == 0)
	{
		std::cout << "No task tomorrow" << std::endl;
	}
}

void TaskManager::printTaskForDate(const std::vector<Task> &tasks, int years, int mon, int day)
{
	int counter = 0;
	std::cout << "-----------------------------Task for day:----------------------" << std::endl;
	for each (Task task in tasks)
	{
		time_t taskdata = task.GetTime();		
		tm ltm_task;
		localtime_s(&ltm_task, &taskdata);			
		if (ltm_task.tm_year == years && ltm_task.tm_mon== mon && ltm_task.tm_mday == day)
		{
			std::cout << "Name:\t" + task.GetName() << std::endl;
			std::cout << "Description:\t" + task.GetDescription() << std::endl;
			std::cout << std::endl;
			counter++;
		}		
	}
	if (counter == 0)
	{
		std::cout << "No task for: "<< years <<"."<<mon<<"."<<day<< std::endl;
	}
}

