// ConsoleApplication1.cpp: ���������� ����� ����� ��� ����������� ����������.
//
#include "stdafx.h"
#include <iostream>
#include "TaskManager.h"
#include "MyFileReader.h"
#include <time.h>
#include <string>

int main()
{
	setlocale(LC_ALL, "Ukrainian");
	std::string fileName= "data.txt";
	std::vector<Task> ListTask;
	TaskManager *temp=new TaskManager();
	Task *task = new Task;
	int year=0, month=0, day=0;	
	MyFileReader file;		
	ListTask = file.LoadDataToClass(fileName);
	temp->printAll(ListTask);
	std::cout << "Add new Task? y/n:" << std::endl;
	std::string feedback;
	std::cin >> feedback;
	if (feedback == "y")
	{
		int id;
		std::string name;
		std::string description;
		std::cout << "Task id:" << std::endl;
		std::cin>>id;
		std::cout << "Task name:" << std::endl;
		std::getline(std::cin >> std::ws, name);
		std::cout << "Task description:" << std::endl;
		std::getline(std::cin >> std::ws, description);
		task->SetId(id);
		task->SetName(name);
		task->SetDescription(description);
		std::cout << "Date for tasks:" << std::endl;
		std::cout << "Enter year: " << std::endl;
		std::cin >> year;
		std::cout << "Enter month: " << std::endl;
		std::cin >> month;
		std::cout << "Enter day: " << std::endl;
		std::cin >> day;
		file.WriteDataToFile(fileName, task, year, month, day);
		std::cout << "Add sucsess!!! New dataset from file:" << std::endl;
		ListTask = file.LoadDataToClass(fileName);
		temp->printAll(ListTask);
	}	
	
	temp->printTaskToday(ListTask);
	temp->printTaskTomorow(ListTask);
	std::cout << "Select task for date? y/n:" << std::endl;
	std::cin >> feedback;
	if (feedback == "y")
	{
		std::cout << "Date for tasks:" << std::endl;
		std::cout << "Enter year: " << std::endl;
		std::cin >> year;
		std::cout << "Enter month: " << std::endl;
		std::cin >> month;
		std::cout << "Enter day: " << std::endl;
		std::cin >> day;
		temp->printTaskForDate(ListTask, year - 1900, month - 1, day);
	}
	delete temp;
	delete task;
    return 0;
}


